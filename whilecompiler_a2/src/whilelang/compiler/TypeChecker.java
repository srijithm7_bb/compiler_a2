// This file is part of the WhileLang Compiler (wlc).
//
// The WhileLang Compiler is free software; you can redistribute
// it and/or modify it under the terms of the GNU General Public
// License as published by the Free Software Foundation; either
// version 3 of the License, or (at your option) any later version.
//
// The WhileLang Compiler is distributed in the hope that it
// will be useful, but WITHOUT ANY WARRANTY; without even the
// implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
// PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public
// License along with the WhileLang Compiler. If not, see
// <http://www.gnu.org/licenses/>
//
// Copyright 2013, David James Pearce.

package whilelang.compiler;

import static java.lang.Integer.max;
import static whilelang.util.SyntaxError.internalFailure;
import static whilelang.util.SyntaxError.syntaxError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import whilelang.ast.Attribute;
import whilelang.ast.Expr;
import whilelang.ast.Expr.RecordAccess;
import whilelang.ast.Expr.Variable;
import whilelang.ast.Stmt;
import whilelang.ast.Type;
import whilelang.ast.WhileFile;
import whilelang.util.Pair;
import whilelang.util.SyntacticElement;

/**
 * <p>
 * Responsible for ensuring that all types are used appropriately. For example,
 * that we only perform arithmetic operations on arithmetic types; that we only
 * access fields in records guaranteed to have those fields, etc.
 * </p>
 *
 * @author David J. Pearce
 *
 */
public class TypeChecker {
	private WhileFile file;
	private WhileFile.MethodDecl method;
	private HashMap<String,WhileFile.MethodDecl> methods;
	private HashMap<String,WhileFile.TypeDecl> types;

	public void check(WhileFile wf) {
		this.file = wf;
		/*this.methods = new HashMap<String,WhileFile.MethodDecl>();
		this.types = new HashMap<String,WhileFile.TypeDecl>(); */
	    this.methods = new HashMap<>();
	    this.types = new HashMap<>();

		for(WhileFile.Decl declaration : wf.declarations) {
			if(declaration instanceof WhileFile.MethodDecl) {
				WhileFile.MethodDecl fd = (WhileFile.MethodDecl) declaration;
				this.methods.put(fd.name(), fd);
			} else if(declaration instanceof WhileFile.TypeDecl) {
				WhileFile.TypeDecl fd = (WhileFile.TypeDecl) declaration;
				this.types.put(fd.name(), fd);
			}
		}

		for(WhileFile.Decl declaration : wf.declarations) {
			if(declaration instanceof WhileFile.TypeDecl) {
				check((WhileFile.TypeDecl) declaration);
			} else if(declaration instanceof WhileFile.MethodDecl) {
				check((WhileFile.MethodDecl) declaration);
			}
		}
	}

	public void check(WhileFile.TypeDecl td) {
		checkNotVoid(td.getType(),td.getType());
	}

	public void check(WhileFile.MethodDecl fd) {
		this.method = fd;

		// First, initialise the typing environment
		//HashMap<String,Type> environment = new HashMap<String,Type>();
		HashMap<String, Type> environment = new HashMap<>();
		for (WhileFile.Parameter p : fd.getParameters()) {
			checkNotVoid(p.getType(),p);
			environment.put(p.name(), p.getType());
		}

		// Second, check all statements in the function body
		check(fd.getBody(),environment);
	}

	public void check(List<Stmt> statements, Map<String,Type> environment) {
		for(Stmt s : statements) {
			check(s,environment);
		}
	}

	public void check(Stmt stmt, Map<String,Type> environment) {
		if(stmt instanceof Stmt.Assert) {
			check((Stmt.Assert) stmt, environment);
		} else if(stmt instanceof Stmt.Assign) {
			check((Stmt.Assign) stmt, environment);
		} else if(stmt instanceof Stmt.Return) {
			check((Stmt.Return) stmt, environment);
		} else if(stmt instanceof Stmt.Break) {
			// nothing to do
		} else if(stmt instanceof Stmt.Continue) {
			// nothing to do
		} else if(stmt instanceof Stmt.VariableDeclaration) {
			check((Stmt.VariableDeclaration) stmt, environment);
		} else if(stmt instanceof Expr.Invoke) {
			check((Expr.Invoke) stmt, false, environment);
		} else if(stmt instanceof Stmt.IfElse) {
			check((Stmt.IfElse) stmt, environment);
		} else if(stmt instanceof Stmt.For) {
			check((Stmt.For) stmt, environment);
		} else if(stmt instanceof Stmt.While) {
			check((Stmt.While) stmt, environment);
		} else if(stmt instanceof Stmt.Switch) {
			check((Stmt.Switch) stmt, environment);
		} else {
			internalFailure("unknown statement encountered (" + stmt + ")", file.filename,stmt);
		}
	}


	public void check(Stmt.VariableDeclaration stmt, Map<String,Type> environment) {
		if(environment.containsKey(stmt.getName())) {
			syntaxError("variable already declared: " + stmt.getName(),
					file.filename, stmt);
		} else if(stmt.getExpr() != null) {
			Type type = check(stmt.getExpr(),environment);
			checkSubtype(stmt.getType(),type,stmt.getExpr());
		}
		environment.put(stmt.getName(), stmt.getType());
	}

	public void check(Stmt.Assert stmt, Map<String,Type> environment) {
		Type t = check(stmt.getExpr(),environment);
		checkInstanceOf(t,stmt.getExpr(),Type.Bool.class);
	}


	public void check(Stmt.Assign stmt, Map<String,Type> environment) {

		Type lhs = check(stmt.getLhs(),environment);
		Type rhs = check(stmt.getRhs(),environment);
		
		// Make sure the type being assigned is a subtype of the destination
		checkSubtype(lhs,rhs,stmt.getRhs());
	}

	public void check(Stmt.Return stmt, Map<String, Type> environment) {
		if(stmt.getExpr() != null) {
			Type ret = check(stmt.getExpr(),environment);
			// Make sure returned value is subtype of enclosing method's return
			// type
			checkSubtype(method.getRet(),ret,stmt.getExpr());
		} else {
			// Make sure return type is instance of Void
			checkInstanceOf(new Type.Void(),stmt,method.getRet().getClass());
		}
	}

	public void check(Stmt.IfElse stmt, Map<String,Type> environment) {
		Type ct = check(stmt.getCondition(),environment);
		// Make sure condition has bool type
		checkInstanceOf(ct,stmt.getCondition(),Type.Bool.class);
		check(stmt.getTrueBranch(),environment);
		check(stmt.getFalseBranch(),environment);
	}

	public void check(Stmt.For stmt, Map<String,Type> environment) {

		Stmt.VariableDeclaration vd = stmt.getDeclaration();
		check(vd,environment);

		// Clone the environment in order that the loop variable is only scoped
		// for the life of the loop itself.
		environment = new HashMap<String,Type>(environment);
		environment.put(vd.getName(), vd.getType());

		Type ct = check(stmt.getCondition(),environment);
		// Make sure condition has bool type
		checkInstanceOf(ct,stmt.getCondition(),Type.Bool.class);
		check(stmt.getIncrement(),environment);
		check(stmt.getBody(),environment);
	}

	public void check(Stmt.While stmt, Map<String,Type> environment) {
		Type ct = check(stmt.getCondition(),environment);
		// Make sure condition has bool type
		checkInstanceOf(ct,stmt.getCondition(),Type.Bool.class);
		check(stmt.getBody(),environment);
	}

	public void check(Stmt.Switch stmt, Map<String,Type> environment) {
		Type ct = check(stmt.getExpr(),environment);
		// Now, check each case individually
		for(Stmt.Case c : stmt.getCases()) {
			if(!c.isDefault()) {
				Type et = check(c.getValue(),environment);
				checkSubtype(ct,et,c.getValue());
			}
			check(c.getBody(),environment);
		}
	}

	public Type check(Expr expr, Map<String,Type> environment) {
		Type type;

		if(expr instanceof Expr.Binary) {
			type = check((Expr.Binary) expr, environment);
		} else if(expr instanceof Expr.Literal) {
			type = check((Expr.Literal) expr, environment);
		} else if(expr instanceof Expr.IndexOf) {
			type = check((Expr.IndexOf) expr, environment);
		} else if(expr instanceof Expr.Invoke) {
			type = check((Expr.Invoke) expr, true, environment);
		} else if(expr instanceof Expr.ArrayGenerator) {
			type = check((Expr.ArrayGenerator) expr, environment);
		} else if(expr instanceof Expr.ArrayInitialiser) {
			type = check((Expr.ArrayInitialiser) expr, environment);
		} else if(expr instanceof Expr.RecordAccess) {
			type = check((Expr.RecordAccess) expr, environment);
		} else if(expr instanceof Expr.RecordConstructor) {
			type = check((Expr.RecordConstructor) expr, environment);
		} else if(expr instanceof Expr.Unary) {
			type = check((Expr.Unary) expr, environment);
		} else if(expr instanceof Expr.Variable) {
			type = check((Expr.Variable) expr, environment);
		} else if (expr instanceof Expr.TypeTests) {
		      type = check((Expr.TypeTests) expr, environment);
		} else if (expr instanceof Expr.Casting) {
		      type = check((Expr.Casting) expr, environment);
		} else {
			internalFailure("unknown expression encountered (" + expr + ")", file.filename,expr);
			return null; // dead code
		}

		// Save the type attribute so that subsequent compiler stages can use it
		// without having to recalculate it from scratch.
		expr.attributes().add(new Attribute.Type(type));

		return type;
	}
	
	public Type check(Expr.TypeTests expr, Map<String, Type> environment) {

		if(expr.getLhs() instanceof Expr.RecordAccess && (expr.getRhs() instanceof Type.Bool  || expr.getRhs() instanceof Type.Int || expr.getRhs() instanceof Type.Null ))
		{
			Expr.RecordAccess ra = (RecordAccess) expr.getLhs();
			Type lhs = environment.get(ra.getSource().toString());
			if(!(isSubtype(expr.getRhs(),lhs,null)))
			{
				syntaxError("impossible cast: from " + expr.getRhs() + " to " + lhs, file.filename, expr);
			}
		}
		
		
		Type lhsType = check(expr.getLhs(), environment);
	    Type rhs = expr.getRhs();
	    if((lhsType instanceof Type.Int || lhsType instanceof Type.Null || lhsType instanceof Type.Bool) &&
	    		(rhs instanceof Type.Int || rhs instanceof Type.Null || rhs instanceof Type.Bool)	)
	    {
	    	if(!lhsType.toString().trim().equals(rhs.toString().trim()))
	    	{
	    		syntaxError("impossible cast: from " + lhsType + " to " + rhs, file.filename, expr);
	    	}
	    }
	    
	    if(lhsType instanceof Type.Union && rhs instanceof Type.Union)
	    {
	    	String[] lhsStr = lhsType.toString().split("\\|");
	    	String[] rhsStr = rhs.toString().split("\\|");
	    	Arrays.sort(lhsStr);
	    	Arrays.sort(rhsStr);
	
	    	if(lhsStr.length != rhsStr.length)
	    	{
	    		syntaxError("impossible cast: from " + lhsType + " to " + rhs, file.filename, expr);
	    	}
	    	if(!Arrays.equals(lhsStr, rhsStr))
	    	{
	    		syntaxError("impossible cast: from " + lhsType + " to " + rhs, file.filename, expr);
	    	}

	    }
	    
	    expr.setLhsType(lhsType);
	    return new Type.Bool();
	  }

	
	public Type check(Expr.Casting expr, Map<String, Type> environment) {
		
	    Expr element = expr.getExpr();

	    Type originalType = check(element, environment);
	    Type castType = expr.getCoercion();
	    

	    // in case of union type we have to check the cast is part of the original union type
	    if (originalType instanceof Type.Union) {
	      if (!(castType instanceof Type.Union)) {
	        if (!(isSubtype(originalType, castType, element))) {
	          syntaxError("impossible cast: from " + originalType.toString() + " to " + castType.toString(), file.filename, expr);
	        } else {
	          return castType;
	        }
	      }

	      if (!(isUnionSubtype(originalType, castType, true, element))) {
	        syntaxError("impossible coercion: from " + originalType.toString() + " to " + castType.toString(), file.filename, expr);
	      } else {
	        return castType;
	      }
	    }

	    if (isValidCasting(originalType, castType)) {
	      return castType;
	    }


	    // cast type should be subtype of original type
	    if (!isSubtype(castType, originalType, element)) {
	      syntaxError("impossible coercion: from " + originalType.toString() + " to " + castType.toString(), file.filename, expr);
	    }

	    return castType;
	  }

	private boolean isValidCasting(Type originalType, Type castType) {
	
		if(originalType instanceof Type.Bool && castType instanceof Type.Int)
		{
			return false;
		}
		if(originalType instanceof Type.Int && castType instanceof Type.Bool)
		{
			return false;
		}
		if(originalType instanceof Type.Array || castType instanceof Type.Array)
		{
			if(!originalType.toString().equals(castType.toString()))
			{
				return false;
			}
		}
		
		
	    return (originalType instanceof Type.Named || castType instanceof Type.Named) ||
	    	        (originalType instanceof Type.Int && castType instanceof Type.Int) ||
	    	        (originalType instanceof Type.Array && castType instanceof Type.Array);
	    
		/*

		if(originalType.toString().contains("bool") && castType.toString().contains("int"))
		{
			return false;
		}
		if(originalType.toString().contains("int") && castType.toString().contains("bool"))
		{
			return false;
		}
		if(originalType instanceof Type.Named && !(castType instanceof Type.Named))
		{
			return false;
		}
		if(castType instanceof Type.Named && !(originalType instanceof Type.Named))
		{
			return false;
		}
		if(originalType instanceof Type.Array || castType instanceof Type.Array)
		{
			if(!originalType.toString().equals(castType.toString()))
			{
				return false;
			}
			return true;
		}
		
		
		return true; */
	}
	

	public Type check(Expr.Binary expr, Map<String,Type> environment) {
		Type leftType = check(expr.getLhs(), environment);
		Type rightType = check(expr.getRhs(), environment);

		switch(expr.getOp()) {
		case AND:
		case OR:
			// Check arguments have bool type
			checkInstanceOf(leftType,expr.getLhs(),Type.Bool.class);
			checkInstanceOf(rightType,expr.getRhs(),Type.Bool.class);
			return leftType;
		case ADD:
		case SUB:
		case DIV:
		case MUL:
		case REM:
			// Check arguments have int type
			checkInstanceOf(leftType,expr.getLhs(),Type.Int.class);
			checkInstanceOf(rightType,expr.getRhs(),Type.Int.class);
			return leftType;
		case EQ:
			//&& (rightType instanceof Type.Bool || rightType instanceof Type.Int)
			if(leftType instanceof Type.Named && (rightType instanceof Type.Bool || rightType instanceof Type.Int))
			{
				if(rightType instanceof Type.Bool) {
				if(!isSubtype(rightType, leftType, null))
				{
					syntaxError("impossible cast: from " , file.filename, expr);
				}
				}
				//syntaxError("impossible cast: from " , file.filename, expr);
			}
			
		case NEQ:
			// FIXME: we could do better here by making sure one of the
			// arguments is a subtype of the other.
			return new Type.Bool();
		case LT:
		case LTEQ:
		case GT:
		case GTEQ:
			// Chewck arguments have int type
			checkInstanceOf(leftType,expr.getLhs(),Type.Int.class);
			checkInstanceOf(rightType,expr.getRhs(),Type.Int.class);
			return new Type.Bool();
		default:
			internalFailure("unknown unary expression encountered (" + expr + ")", file.filename,expr);
			return null; // dead code
		}
	}

	public Type check(Expr.Literal expr, Map<String,Type> environment) {
		return typeOf(expr.getValue(),expr);
	}

	public Type check(Expr.IndexOf expr, Map<String, Type> environment) {
		Type srcType = check(expr.getSource(), environment);
		Type indexType = check(expr.getIndex(), environment);
		// Make sure index has integer type
		checkInstanceOf(indexType, expr.getIndex(), Type.Int.class);
		// Check src has array type (of some kind)
		srcType = checkInstanceOf(srcType, expr.getSource(), Type.Array.class);
		return ((Type.Array) srcType).getElement();
	}

	public Type check(Expr.Invoke expr, boolean returnRequired, Map<String,Type> environment) {
		WhileFile.MethodDecl fn = methods.get(expr.getName());
		List<Expr> arguments = expr.getArguments();
		List<WhileFile.Parameter> parameters = fn.getParameters();
		if(arguments.size() != parameters.size()) {
			syntaxError("incorrect number of arguments to function",
					file.filename, expr);
		}
		for(int i=0;i!=parameters.size();++i) {
			Type argument = check(arguments.get(i),environment);
			Type parameter = parameters.get(i).getType();
			// Check supplied argument is subtype of declared parameter
			checkSubtype(parameter,argument,arguments.get(i));
		}
		Type returnType = fn.getRet();
		if(returnRequired) {
			checkNotVoid(returnType,fn.getRet());
		}
		return returnType;
	}

	public Type check(Expr.ArrayGenerator expr, Map<String, Type> environment) {
		Type element = check(expr.getValue(), environment);
		Type size = check(expr.getSize(), environment);
		// Check size expression has int type
		checkInstanceOf(size,expr.getSize(),Type.Int.class);
		return new Type.Array(element);
	}

	  public boolean verifyAllEqualUsingALoop(ArrayList<Type> list) {
		    for (Type s : list) {
		        if (!s.toString().equals(list.get(0).toString()))
		            return false;
		    }
		    return true;
		}
	public Type check(Expr.ArrayInitialiser expr, Map<String, Type> environment) {
		ArrayList<Type> types = new ArrayList<Type>();
		List<Expr> arguments = expr.getArguments();
		for (Expr argument : arguments) {
			types.add(check(argument, environment));
		}

		/*
		 * if(!verifyAllEqualUsingALoop(types)) { Type t= new Type.Bool(); return t; }
		 */

		Type lub = leastUpperBound(types,expr);
		return new Type.Array(lub);
	}

	public Type check(Expr.RecordAccess expr, Map<String, Type> environment) {
		Type srcType = check(expr.getSource(), environment);
		// Check src has record type
		Type.Record recordType = (Type.Record) checkInstanceOf(srcType, expr.getSource(), Type.Record.class);
		/*for (Pair<Type, String> field : recordType.getFields()) {
			if (field.second().equals(expr.getName())) {
				return field.first();
			}*/
		Set<Type> typeSet = recordType.getTypesByFieldName(expr.getName());

	    if (typeSet.size() == 1) {
	      return typeSet.stream().findFirst().get();
	    } else {
	      return new Type.Union(new ArrayList(typeSet));
		}
		// Couldn't find the field!
		/*
		syntaxError("expected type to contain field: " + expr.getName(), file.filename, expr);
		return null; // deadcode
		*/
	}

	public Type check(Expr.RecordConstructor expr, Map<String, Type> environment) {
	    List<Pair<String, Expr>> fields = expr.getFields();
	    List<Pair<Set<Type>, String>> fieldsWithTypes = new ArrayList<>();

	    for (Pair<String, Expr> field : fields) {
	      String fieldName = field.first();
	      Expr expression = field.second();

	      Type typeOfRecordExpression = check(expression, environment);
	      final Set<Type> typeOfRecordExpressionSet = new HashSet<>();
	      typeOfRecordExpressionSet.add(typeOfRecordExpression);
	      Pair<Set<Type>, String> pair = new Pair<>(typeOfRecordExpressionSet, fieldName);
	      fieldsWithTypes.add(pair);
		}

	    return new Type.Record(fieldsWithTypes);
	}

	public Type check(Expr.Unary expr, Map<String,Type> environment) {
		Type type = check(expr.getExpr(), environment);
		switch(expr.getOp()) {
		case NEG:
			checkInstanceOf(type,expr.getExpr(),Type.Int.class);
			return type;
		case NOT:
			checkInstanceOf(type,expr.getExpr(),Type.Bool.class);
			return type;
		case LENGTHOF:
			checkInstanceOf(type,expr.getExpr(),Type.Array.class);
			return new Type.Int();
		default:
			internalFailure("unknown unary expression encountered (" + expr + ")", file.filename,expr);
			return null; // dead code
		}
	}

	public Type check(Expr.Variable expr, Map<String, Type> environment) {
		Type type = environment.get(expr.getName());
		if (type == null) {
			syntaxError("unknown variable encountered: " + expr.getName(),
					file.filename, expr);
		}
		return type;
	}

	/**
	 * Determine the type of a constant value
	 *
	 * @param constant
	 * @param elem
	 * @return
	 */
	private Type typeOf(Object constant, SyntacticElement elem) {
		if (constant instanceof Boolean) {
			return new Type.Bool();
		} else if (constant instanceof Integer) {
			return new Type.Int();
		} else if (constant instanceof Character) {
			return new Type.Int();
		} else if (constant == "nullConstant") {
			return new Type.Null();
		} else if (constant instanceof String) {
			return new Type.Array(new Type.Int());
		} else if (constant instanceof ArrayList) {
			ArrayList<Object> list = (ArrayList) constant;
			ArrayList<Type> types = new ArrayList<Type>();
			for(Object o : list) {
				types.add(typeOf(o,elem));
			}
			Type lub = leastUpperBound(types,elem);
			return new Type.Array(lub);
		} else if (constant instanceof HashMap) {
			HashMap<String, Object> record = (HashMap<String, Object>) constant;
			//ArrayList<Pair<Type, String>> fields = new ArrayList<Pair<Type, String>>();
			ArrayList<Pair<Set<Type>, String>> fields = new ArrayList<>();
			
			// FIXME: there is a known bug here related to the ordering of
			// fields. Specifically, we've lost information about the ordering
			// of fields in the original source file and we are just recreating
			// a random order here.
			for (Map.Entry<String, Object> e : record.entrySet()) {
				Type t = typeOf(e.getValue(), elem);
				//fields.add(new Pair<Type, String>(t, e.getKey()));
				Set<Type> typeSet = new HashSet<>();
		        typeSet.add(t);
		        fields.add(new Pair<Set<Type>, String>(typeSet, e.getKey()));
			}
			return new Type.Record(fields);
		} else {
			internalFailure("unknown constant encountered (" + elem + ")", file.filename, elem);
			return null; // dead code
		}
	}

	private Type leastUpperBound(List<Type> types, SyntacticElement elem) {
		Type lub = new Type.Void();
		
		for (Type t : types) {
			if (isSubtype(t, lub, elem)) {
				lub = t;
			} else {
				if((t instanceof Type.Null) || (t instanceof Type.Record) || (t instanceof Type.Array) || (t instanceof Type.Int) )
				{
					
				}else {
			
					checkSubtype(lub, t, elem);}
				}
			
		}
		return lub;
	}

	/**
	 * Check that a given type t2 is an instance of of another type t1. This
	 * method is useful for checking that a type is, for example, a List type.
	 *
	 * @param t1
	 * @param type
	 * @param element
	 *            Used for determining where to report syntax errors.
	 * @return
	 */
	public Type checkInstanceOf(Type type,
			SyntacticElement element, Class<?>... instances) {

		if(type instanceof Type.Named) {
			Type.Named tn = (Type.Named) type;
			if (types.containsKey(tn.getName())) {
				Type body = types.get(tn.getName()).getType();
				return checkInstanceOf(body, element, instances);
			} else {
				syntaxError("unknown type encountered: " + type, file.filename,
						element);
			}
		}
		for (Class<?> instance : instances) {
			if (instance.isInstance(type)) {
				// This cast is clearly unsafe. It relies on the caller of this
				// method to do the right thing.
				return type;
			}
		}

		// Ok, we're going to fail with an error message. First, let's build up
		// a useful human-readable message.

		String msg = "";
		boolean firstTime = true;
		for (Class<?> instance : instances) {
			if(!firstTime) {
				msg = msg + " or ";
			}
			firstTime=false;

			if (instance.getName().endsWith("Bool")) {
				msg += "bool";
			} else if (instance.getName().endsWith("Char")) {
				msg += "char";
			} else if (instance.getName().endsWith("Int")) {
				msg += "int";
			} else if (instance.getName().endsWith("Strung")) {
				msg += "string";
			} else if (instance.getName().endsWith("Array")) {
				msg += "array";
			} else if (instance.getName().endsWith("Record")) {
				msg += "record";
			} else if (instance.getName().endsWith("Void")) {
				msg += "void";
			} else if (instance.getName().endsWith("Null")) {
				msg += "null";
			} else {
				internalFailure("unknown type instanceof encountered ("
						+ instance.getName() + ")", file.filename, element);
				return null;
			}
		}

		syntaxError("expected instance of " + msg + ", found " + type,
				file.filename, element);
		return null;
	}

	/**
	 * Check that a given type t2 is a subtype of another type t1.
	 *
	 * @param t1
	 *            Supertype to check
	 * @param t2
	 *            Subtype to check
	 * @param element
	 *            Used for determining where to report syntax errors.
	 */
	public void checkSubtype(Type t1, Type t2, SyntacticElement element) {
		if(!t1.toString().equals(t2.toString()))
		{
			if(!isSubtype(t1,t2,element)) {
				syntaxError("expected type " + t1 + ", found " + t2, file.filename,
						element);
			}
		}
	}

	/**
	 * Check that a given type t2 is a subtype of another type t1.
	 *
	 * @param t1
	 *            Supertype to check
	 * @param t2
	 *            Subtype to check
	 * @param element
	 *            Used for determining where to report syntax errors.
	 */
	public boolean isSubtype(Type t1, Type t2, SyntacticElement element) {
		if (t2 instanceof Type.Void) {
			// OK
			return true;
		} else if (t1 instanceof Type.Bool && t2 instanceof Type.Bool) {
			// OK
			return true;
		} else if (t1 instanceof Type.Int && t2 instanceof Type.Int) {
			// OK
			return true;
		} else if (t1 instanceof Type.Null && t2 instanceof Type.Null) {
			// OK
			return true;
		} else if (t1 instanceof Type.Union && t2 instanceof Type.Union) {
		      return isUnionSubtype(t1, t2, false, element);
	    } else if ((t1 instanceof Type.Bool && t2 instanceof Type.Null) || 
	    		(t1 instanceof Type.Null && t2 instanceof Type.Bool)) {
			// OK
			return false;
		} else if ((t1 instanceof Type.Bool && t2 instanceof Type.Int) || 
	    		(t1 instanceof Type.Int && t2 instanceof Type.Bool)) {
			// OK
			return false;
		} else if ((t1 instanceof Type.Null && t2 instanceof Type.Int) || 
	    		(t1 instanceof Type.Int && t2 instanceof Type.Null)) {
			// OK
			return false;
		} else if (t1 instanceof Type.Union) {
	      // OK
	    	int count=0;
	      for (Type type : ((Type.Union) t1).getTypes()) {
	        if (isSubtype(type, t2, element)) {
	        	count =1;
	          return true;
	        }
	      }
	      if(count==0)
	      {
	    	  return false;
	      }
	    } else if (t2 instanceof Type.Union) {
	    	if((t1 instanceof Type.Int) || (t1 instanceof Type.Bool) || (t1 instanceof Type.Null))
	    	{
	    		return false;
	    	}
		      // OK
	    	int count=0;
		      for (Type type : ((Type.Union) t2).getTypes()) {
		        if (isSubtype(type, t1, element)) {
		        	count=1;
		          return true;
		        }
		      }
		      if(count==0)
		      {
		    	  return false;
		      }
		    } else if (t1 instanceof Type.Array && t2 instanceof Type.Array) {
			Type.Array l1 = (Type.Array) t1;
			Type.Array l2 = (Type.Array) t2;
			// The following is safe because While has value semantics. In a
			// conventional language, like Java, this is not safe because of
			// references.
			return isSubtype(l1.getElement(),l2.getElement(),element);
		} else if (t1 instanceof Type.Record && t2 instanceof Type.Record) {
			Type.Record r1 = (Type.Record) t1;
			Type.Record r2 = (Type.Record) t2;

			if (r1.getFieldSize() > r2.getFieldSize()) {
				return false;
			}
		      
		      List<String> r1FieldNames = r1.getFieldNames();
		      List<String> r2FieldNames = r2.getFieldNames();
		      AtomicInteger foundMatch = new AtomicInteger(0);

		try {      
		      r1FieldNames.forEach(r1FieldName -> {

		        Set<Type> r1FieldTypes = r1.getTypesByFieldName(r1FieldName);
		        Set<Type> r2FieldTypes = r2.getTypesByFieldName(r1FieldName);

		        //Iterator<Type> r1Iter = r1FieldTypes.iterator();
		        int flag=0;
		        for (Type r1Iter : r1FieldTypes) {
		        	for (Type r2Iter : r2FieldTypes) {
			        	if(r2Iter.toString().equals(r1Iter.toString()))
			        	{
			        		flag=1;
			        	}
			        }
		        }

		        if(flag==0)
		        {
		        	foundMatch.addAndGet(500);
		        }
		        
		        
		        int count =0;
		        
		        for (Type r1Iter : r1FieldTypes) {
		        	for (Type r2Iter : r2FieldTypes) {
		        		if (isSubtype(r1Iter, r2Iter, element))
			              {
			            	  	count=1;
			              }
			        }
		        }
		        
		        if(count==0)
		        {
		        	foundMatch.addAndGet(999);
		        }
		        
		      }); 
		}
		catch(Exception e )
		{
		      
		      for(String r1field : r1FieldNames)
		      {
		    	  for(String r2field : r2FieldNames)
			      {
			    	  Set<Type> r1FieldTypes = r1.getTypesByFieldName(r1field);
				      Set<Type> r2FieldTypes = r2.getTypesByFieldName(r2field);
				      int count =0;
				        
				        for (Type r1Iter : r1FieldTypes) {
				        	for (Type r2Iter : r2FieldTypes) {
				        		if (isSubtype(r1Iter, r2Iter, element))
					              {
					            	  	count=1;
					              }
					        }
				        }
				        

				        if(count==0)
				        {
				        	foundMatch.addAndGet(999);
				        }
			      }
		      }

		      if(foundMatch.get() > 900)
		      {
		    	  
		    	  return false;
		      }
		      else {
		    	  return true;
		      }
		}
		
		if(foundMatch.get() > 1000)
	      {
	    	  
	    	  return false;
	      }
	      else {
	    	  return true;
	      }

		} else if (t1 instanceof Type.Named) {
			Type.Named tn = (Type.Named) t1;
			if (types.containsKey(tn.getName())) {
				Type body = types.get(tn.getName()).getType();
				return isSubtype(body, t2, element);
			} else {
				syntaxError("unknown type encountered: " + t1, file.filename,
						element);
			}
		} else if (t2 instanceof Type.Named) {
			Type.Named tn = (Type.Named) t2;
			if (types.containsKey(tn.getName())) {
				Type body = types.get(tn.getName()).getType();
				return isSubtype(body, t1, element);
			} else {
				syntaxError("unknown type encountered: " + t2, file.filename,
						element);
			}
		}else if (t1 instanceof Type.Record && (t2 instanceof Type.Int || t2 instanceof Type.Bool || t2 instanceof Type.Null)) {
			
			Type.Record r1 = (Type.Record) t1;

		      
		      List<String> r1FieldNames = r1.getFieldNames();
		      AtomicInteger foundMatch = new AtomicInteger(0);

		try {      
		      r1FieldNames.forEach(r1FieldName -> {

		        Set<Type> r1FieldTypes = r1.getTypesByFieldName(r1FieldName);

		        //Iterator<Type> r1Iter = r1FieldTypes.iterator();
		        int flag=0;
		        for (Type r1Iter : r1FieldTypes) {
			        	if(t2.toString().equals(r1Iter.toString()))
			        	{
			        		flag=1;
			        	}
		        }

		        if(flag==0)
		        {
		        	foundMatch.addAndGet(500);
		        }
		        
		        
		        int count =0;
		        
		        for (Type r1Iter : r1FieldTypes) {
		        		if (isSubtype(r1Iter, t2, element))
			              {
			            	  	count=1;
			              }
		        }
		        
		        if(count==0)
		        {
		        	foundMatch.addAndGet(999);
		        }
		        
		      }); 
		}
		catch(Exception e )
		{
		      
		      for(String r1field : r1FieldNames)
		      {
			    	  Set<Type> r1FieldTypes = r1.getTypesByFieldName(r1field);
				      int count =0;
				        
				        for (Type r1Iter : r1FieldTypes) {
				        		if (isSubtype(r1Iter, t2, element))
					              {
					            	  	count=1;
					              }
				        }
				        

				        if(count==0)
				        {
				        	foundMatch.addAndGet(999);
				        }
		      }

		      if(foundMatch.get() > 900)
		      {
		    	  
		    	  return false;
		      }
		      else {
		    	  return true;
		      }
		}
		
		if(foundMatch.get() > 1000)
	      {
	    	  
	    	  return false;
	      }
	      else {
	    	  return true;
	      }
		
			
		}else if (t2 instanceof Type.Record && (t1 instanceof Type.Int || t1 instanceof Type.Bool || t1 instanceof Type.Null)) {
			
			Type.Record r1 = (Type.Record) t2;

		      
		      List<String> r1FieldNames = r1.getFieldNames();
		      AtomicInteger foundMatch = new AtomicInteger(0);

		try {      
		      r1FieldNames.forEach(r1FieldName -> {

		        Set<Type> r1FieldTypes = r1.getTypesByFieldName(r1FieldName);

		        //Iterator<Type> r1Iter = r1FieldTypes.iterator();
		        int flag=0;
		        for (Type r1Iter : r1FieldTypes) {
			        	if(t2.toString().equals(r1Iter.toString()))
			        	{
			        		flag=1;
			        	}
		        }

		        if(flag==0)
		        {
		        	foundMatch.addAndGet(500);
		        }
		        
		        
		        int count =0;
		        
		        for (Type r1Iter : r1FieldTypes) {
		        		if (isSubtype(r1Iter, t1, element))
			              {
			            	  	count=1;
			              }
		        }
		        
		        if(count==0)
		        {
		        	foundMatch.addAndGet(999);
		        }
		        
		      }); 
		}
		catch(Exception e )
		{
		      
		      for(String r1field : r1FieldNames)
		      {
			    	  Set<Type> r1FieldTypes = r1.getTypesByFieldName(r1field);
				      int count =0;
				        
				        for (Type r1Iter : r1FieldTypes) {
				        		if (isSubtype(r1Iter, t1, element))
					              {
					            	  	count=1;
					              }
				        }
				        

				        if(count==0)
				        {
				        	foundMatch.addAndGet(999);
				        }
		      }

		      if(foundMatch.get() > 900)
		      {
		    	  
		    	  return false;
		      }
		      else {
		    	  return true;
		      }
		}
		
		if(foundMatch.get() > 1000)
	      {
	    	  
	    	  return false;
	      }
	      else {
	    	  return true;
	      }
	
		}
		else {
			return false;
		}
		return true;
	}
	
	  public boolean isUnionSubtype(Type t1, Type t2, boolean isCoercion, SyntacticElement element) {
		  	int unionT1Size = ((Type.Union) t1).getTypes().size();
		    int unionT2Size = ((Type.Union) t2).getTypes().size();
		    int typesSize = max(unionT1Size, unionT2Size);
		    int numberOfFoundSubtypes = 0;

		    for (Type type1 : ((Type.Union) t1).getTypes()) {
		      boolean foundASubtypeForType1 = false;
		      for (Type type2 : ((Type.Union) t2).getTypes()) {
		        if (isSubtype(type1, type2, element)) {
		          foundASubtypeForType1 = true;
		        }
		      }
		      if (foundASubtypeForType1) {
		        numberOfFoundSubtypes++;
		      }
		    }

		    // in coercion a subset of the original type enough to keep type compatibility
		    if (isCoercion) {
		      return numberOfFoundSubtypes == unionT2Size;
		    } else {
		      return numberOfFoundSubtypes == typesSize;
		    }
		  }


	/**
	 * Determine whether two given types are euivalent. Identical types are always
	 * equivalent. Furthermore, e.g. "int|null" is equivalent to "null|int".
	 *
	 * @param t1
	 *            first type to compare
	 * @param t2
	 *            second type to compare
	 */
	public boolean equivalent(Type t1, Type t2, SyntacticElement element) {
		return isSubtype(t1,t2,element) && isSubtype(t2,t1,element);
	}

	/**
	 * Check that a given type is not equivalent to void. This is because void
	 * cannot be used in certain situations.
	 *
	 * @param t
	 * @param elemt
	 */
	public void checkNotVoid(Type t, SyntacticElement elem) {
		if(t instanceof Type.Void) {
			syntaxError("void type not permitted here",file.filename,elem);
		} else if(t instanceof Type.Record) {
			Type.Record r = (Type.Record) t;
			for (Pair<Set<Type>, String> field : r.getFields()) {
		        field.first().forEach(type -> checkNotVoid(type, elem));
			}
		} else if(t instanceof Type.Array) {
			Type.Array at = (Type.Array) t;
			checkNotVoid(at.getElement(),at.getElement());
		}
	}
}
